Benchmarks of Mopsa on Juliet Test Suite
========================================

This repository contains the scripts to run Mopsa on Juliet Test Suite for C/C++.


Quick Start
-----------
Before starting the analysis, first download the sources of Juliet by issuing:
```shell
$ make prepare
```

To run the analysis of all (supported) test cases:
```shell
$ make
```
It is possible to run `make` in parallel by adding the option `-j<N>` with some maximal number of parallel jobs.

Mopsa will analyze every good and bad versions of the tests.
For each case, four outcomes are possible:
1. If no alarm is found in the good test, and one alarm is found in the bad test correspondin to the CWE, the analysis is *successful*.
2. If no alarm corresponding to the to the CWE is found in the bad test, the analysis is *unsound*.
3. If the analysis reports alarms in the good test or more than one alarm in the bad case, the analysis is *imprecise*.
4. Finally, if the program contains features that can't be analyzed by Mopsa, the test is *unsupported*.


Note that it is possible to run an individual test:
```shell
$ make src/C/testcases/CWE476_NULL_Pointer_Dereference/CWE476_NULL_Pointer_Dereference__int_01.exe
```
or all tests of a single CWE:
```shell
$ make src/C/testcases/CWE476_NULL_Pointer_Dereference.exe.all
```

JSON
----

Mopsa can save the results of the analysis in JSON files by executing:
```shell
$ make json OUTPUT=<path-to-output>
```
where `<path-to-output>` is the path to the output directory where JSON files will be saved.
JSON files for individual tests can also be creating similary to `.exe` and `.exe.all` targets:
```shell
$ make src/C/testcases/CWE190_Integer_Overflow.json.all OUTPUT=<path-to-output>
```

After obtaining the JSON files, it is possible to display a summary of the results:
```shell
$ ./support/stats.py <path-to-output>
```

Coverage
--------
Not all tests are considered.
Mopsa targets only CWEs related to runtime errors representing undefined behaviors, as defined by the C Standard.
The list of supported CWEs is (see `support/cwe.txt`):

| ID     | Title                                              |
| ------ | -------------------------------------------------- |
| CWE121 | Stack-based Buffer Overflow                        |
| CWE122 | Heap-based Buffer Overflow                         |
| CWE124 | Buffer Underwrite                                  |
| CWE126 | Buffer Over-read                                   |
| CWE127 | Buffer Under-read                                  |
| CWE190 | Integer Overflow                                   |
| CWE191 | Integer Underflow                                  |
| CWE369 | Divide By Zero                                     |
| CWE415 | Double Free                                        |
| CWE416 | Use After Free                                     |
| CWE469 | Use of Pointer Subtraction to Determine Size       |
| CWE476 | NULL Pointer Dereference                           |
| CWE690 | Unchecked Return Value to NULL Pointer Dereference |


All C files present in these directories are analyzed, except some particular cases which are not related to runtime errors.
The excluded tests can found in `support/excluded.txt`.
