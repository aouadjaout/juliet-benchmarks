# Docker image to run Mospa on Juliet Test Suite
# Build with: docker build -t mopsa-juliet .
#
###############################################

FROM ubuntu:latest


# configuration: required packages
##

ENV APT_DEPS build-essential lsb-release m4 llvm-7-dev libclang-7-dev \
    	     clang-7 libgmp-dev libmpfr-dev zlib1g-dev pkg-config wget  \
	     unzip bubblewrap git python3-requests
ENV OPAM_DEPS apron zarith menhir yojson javalib

# install system packages as root
# create a mopsa user
##

ENV TERM xterm-256color

RUN \
    apt-get update && \
    DEBIAN_FRONTEND=noninteractive apt-get install -y $APT_DEPS && \
    adduser --disabled-password --gecos 'Mopsa' mopsa


# install opam
##

COPY support /tmp/support
RUN sh /tmp/support/install-opam.sh
    

# log in as mopsa
#

USER mopsa
WORKDIR /home/mopsa
ENV LANG C.UTF-8
ENV LC_ALL C.UTF-8


# install OCaml dependencies with opam as mopsa user
##

RUN \
    opam init --disable-sandboxing -y && \
    eval $(opam env) && \
    opam install -y -j 8 $OPAM_DEPS && \
    echo && \
    echo "All done!" && \
    uname -a && \
    cat /etc/debian_version && \    
    lsb_release -d && \
    echo opam `opam --version` && \
    ocamlc -v

# install Juliet
##

COPY Makefile /tmp
WORKDIR /tmp
RUN \
    make prepare && \
    mv src /home/mopsa/juliet
